package ru.garyk.recept;

import android.app.Activity;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Sokolov on 13.05.2015.
 */
public class TagsAdapter extends ArrayAdapter{

    private final FragmentActivity context;
    private final TextView selectedTitle;
    private final TextView resultTitle;

    public TagsAdapter(FragmentActivity context, TextView selectedTitle, TextView resultTitle) {
        super(context, R.layout.tag_list, SearchFragment.selectedItems.selectedTags);
        this.context = context;
        this.selectedTitle = selectedTitle;
        this.resultTitle = resultTitle;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.tag_list, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        txtTitle.setText(SearchFragment.selectedItems.selectedTags.get(position));

        TagObjectHolder holder = new TagObjectHolder();
        holder.deleteView = (RelativeLayout) rowView.findViewById(R.id.trash);
        holder.mainView = (LinearLayout) rowView.findViewById(R.id.tag);
        holder.listView = (ListView) parent;

        rowView.setOnTouchListener(new Swipe(holder, position));

        return rowView;
    }

    public static class TagObjectHolder {
        public LinearLayout mainView;
        public RelativeLayout deleteView;
        public ListView listView;
    }

    class Swipe implements View.OnTouchListener {

        private float downX, upX;
        private static final int MIN_DISTANCE = 300;
        private static final int MIN_LOCK_DISTANCE = 30; // disallow motion intercept
        private boolean motionInterceptDisallowed = false;

        private TagObjectHolder holder;
        private int position;

        public Swipe(TagObjectHolder h, int pos) {
            holder = h;
            position = pos;
        }

        private void swipe(int distance) {
            View animationView = holder.mainView;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animationView.getLayoutParams();
            params.rightMargin = -distance;
            params.leftMargin = distance;
            animationView.setLayoutParams(params);
        }

        private void swipeRemove() {

            //if(SearchFragment.selectedItems.selectedIds.size() > position){
                SearchFragment.selectedItems.selectedTags.remove(position);
                SearchFragment.selectedItems.selectedIds.remove(position);
            //}

            notifyDataSetChanged();

            FragmentTransaction frTransaction = context.getSupportFragmentManager().beginTransaction();
            if(SearchFragment.selectedItems.selectedIds.size() > 0){
                frTransaction.replace(R.id.searched_items, ListItemFragment.getSearchInstance(SearchFragment.selectedItems.selectedIds));
            }else{
                selectedTitle.setVisibility(View.INVISIBLE);
                resultTitle.setVisibility(View.INVISIBLE);
                frTransaction.remove(context.getSupportFragmentManager().findFragmentById(R.id.searched_items));
            }

            ItemFragment.setListViewHeightBasedOnChildren(holder.listView);

            frTransaction.commit();

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    return true;
                }
                case MotionEvent.ACTION_MOVE: {

                    upX = event.getX();
                    float deltaX = downX - upX;

                    if (Math.abs(deltaX) > MIN_LOCK_DISTANCE && holder.mainView != null && !motionInterceptDisallowed) {
                        holder.mainView.requestDisallowInterceptTouchEvent(true);
                        motionInterceptDisallowed = true;
                    }

                    if (deltaX > 0) {
                        holder.deleteView.setVisibility(View.GONE);
                    } else {
                        holder.deleteView.setVisibility(View.VISIBLE);
                    }

                    swipe(-(int) deltaX);

                    return true;
                }
                case MotionEvent.ACTION_UP: {

                    upX = event.getX();
                    float deltaX = upX - downX;
                    if (Math.abs(deltaX) > MIN_DISTANCE) {

                        swipeRemove();
                    } else {
                        swipe(0);
                    }

                    if (holder.mainView != null) {
                        holder.mainView.requestDisallowInterceptTouchEvent(false);
                        motionInterceptDisallowed = false;
                    }

                    holder.deleteView.setVisibility(View.VISIBLE);
                    return true;
                }
                case MotionEvent.ACTION_CANCEL: {
                    holder.deleteView.setVisibility(View.VISIBLE);
                    return false;
                }
            }
            return true;
        }
    }
}
