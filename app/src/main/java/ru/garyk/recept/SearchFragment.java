package ru.garyk.recept;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ru.garyk.recept.SearchFragment.SearchCallbacks} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#getInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment implements ListItemFragment.ListItemCallbacks, Runnable{
    // TODO: Rename parameter arguments, choose names that match

    private SQLiteDatabase sdb;
    private SearchCallbacks mListener;
    private View view;
    private EditText edit;
    private ArrayList<String> tags = new ArrayList<String>();
    private ArrayList<Integer> ids = new ArrayList<Integer>();
    public static final String F_TAG = "search";
    public static ItemsSelected selectedItems;
    private Integer typeSearch = 0;
    private Editable findText;
    private Object lock = new Object();

    // TODO: Rename and change types and number of parameters
    public static SearchFragment getInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_search, container, false);

        edit = (EditText) view.findViewById(R.id.edit);

        final ListView listTags = (ListView) view.findViewById(R.id.tags);
        final TextView selectedTitle = (TextView) view.findViewById(R.id.selected_title);
        final TextView resultTitle = (TextView) view.findViewById(R.id.result_title);
        final ListView listSelectedTags = (ListView) view.findViewById(R.id.selected_tags);
        final MyAutoCompleteTextView autoCompleteTextView = (MyAutoCompleteTextView) view.findViewById(R.id.auto_complete);

        autoCompleteTextView.setAdapter(new SearchNameAdapter(getActivity()));
        autoCompleteTextView.setThreshold(3);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SearchNameAdapter.Recipe recipe = (SearchNameAdapter.Recipe) parent.getItemAtPosition(position);
                onListItemSelected(recipe.getId(), recipe.getTitle());
                autoCompleteTextView.getText().clear();
            }
        });

        selectedItems = new ItemsSelected();

        resume();

        final Button type = (Button) view.findViewById(R.id.type);

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence typeChar[] = new CharSequence[] {getString(R.string.type_ingr), getString(R.string.type_name)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setItems(typeChar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    ((FrameLayout) view.findViewById(R.id.searched_items)).removeAllViews();
                    listSelectedTags.setAdapter(null);

                    if(typeChar[which].equals(getString(R.string.type_name))) {
                        edit.setVisibility(View.GONE);
                        autoCompleteTextView.setVisibility(View.VISIBLE);
                        type.setText(getString(R.string.type_name));
                        selectedTitle.setVisibility(View.GONE);
                        listSelectedTags.setVisibility(View.GONE);
                        resultTitle.setVisibility(View.GONE);
                        typeSearch = 1;
                    }else {
                        autoCompleteTextView.setVisibility(View.GONE);
                        edit.setVisibility(View.VISIBLE);
                        type.setText(getString(R.string.type_ingr));
                        listSelectedTags.setVisibility(View.VISIBLE);
                        resultTitle.setVisibility(View.GONE);
                        typeSearch = 0;

                        resume();
                    }
                    edit.getText().clear();

                    }
                });
                builder.show();
            }
        });

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                findText = s;

                if(s.length() > 2)
                    getActivity().runOnUiThread(SearchFragment.this);

            }
        });

        return view;
    }

    @Override
    public void run(){

        synchronized (lock) {

            ids.clear();
            tags.clear();

            final ListView listTags = (ListView) view.findViewById(R.id.tags);
            final TextView selectedTitle = (TextView) view.findViewById(R.id.selected_title);
            final TextView resultTitle = (TextView) view.findViewById(R.id.result_title);
            final ListView listSelectedTags = (ListView) view.findViewById(R.id.selected_tags);

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.category_list, R.id.txt, tags);

            listTags.setAdapter(adapter);

            if (findText.length() > 2) {

                listTags.setVisibility(View.VISIBLE);
                listSelectedTags.setVisibility(View.VISIBLE);
                try {
                    DatabaseHelper db = new DatabaseHelper(getActivity());
                    sdb = db.getReadableDatabase();

                    String query = "SELECT * FROM tags WHERE value LIKE ? or value LIKE ?";
                    Cursor cursor = sdb.rawQuery(query, new String[]{"%" + findText.toString() + "%", "%" + findText.toString().substring(0, 1).toUpperCase() + findText.toString().substring(1) + "%"});

                    cursor.moveToFirst();
                    while (cursor.isAfterLast() == false) {
                        tags.add(cursor.getString(cursor.getColumnIndex("value")));
                        ids.add(cursor.getInt(cursor.getColumnIndex("id")));
                        cursor.moveToNext();
                    }
                } catch (SQLiteException e) {
                    Log.e(getClass().getSimpleName(), "Could not create or Open the database");
                } finally {
                    sdb.close();
                }

            }

            final ArrayAdapter<String> selectedAdapter = new TagsAdapter(getActivity(), selectedTitle, resultTitle);
            listSelectedTags.setAdapter(selectedAdapter);

            listTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    //hide keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);

                    selectedTitle.setVisibility(View.VISIBLE);

                    selectedItems.selectedTags.add(tags.get(position));
                    selectedItems.selectedIds.add(ids.get(position));
                    Toast.makeText(getActivity(), R.string.added_tag, Toast.LENGTH_LONG).show();
                    listTags.setVisibility(View.GONE);

                    if (selectedItems.selectedIds.size() > 0) {
                        resultTitle.setVisibility(View.VISIBLE);

                        ListItemFragment.F_TAG = selectedItems.selectedIds.toString();

                        Fragment fragmentListItem = getFragmentManager().findFragmentByTag(ListItemFragment.F_TAG);
                        if (fragmentListItem == null) {
                            fragmentListItem = ListItemFragment.getSearchInstance(selectedItems.selectedIds);
                        }
                        FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
                        frTransaction.replace(R.id.searched_items, fragmentListItem, ListItemFragment.F_TAG);
                        frTransaction.commit();

                    } else {
                        resultTitle.setVisibility(View.GONE);
                    }

                    edit.getText().clear();

                    ItemFragment.setListViewHeightBasedOnChildren(listSelectedTags);
                }
            });
        }

    }

    @Override
    public void onListItemSelected(Integer id, String title) {
        ItemFragment.F_TAG = title;

        Fragment  fragmentItem = getFragmentManager().findFragmentByTag(ItemFragment.F_TAG);
        if(fragmentItem == null){
            fragmentItem = ItemFragment.getInstance(id);
        }

        FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
        frTransaction.replace(R.id.container, fragmentItem, ItemFragment.F_TAG);
        frTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        frTransaction.addToBackStack(ListItemFragment.F_TAG);
        frTransaction.commit();
    }

    public static class ItemsSelected{
        public ArrayList<Integer> selectedIds = new ArrayList<>();
        public ArrayList<Integer> selectedRecipesIds = new ArrayList<>();
        public ArrayList<String> selectedTags = new ArrayList<>();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSearchSelected(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SearchCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void resume(){

        selectedItems.selectedIds.clear();
        selectedItems.selectedTags.clear();
        selectedItems.selectedRecipesIds.clear();

        String tags = StateHelper.getValue(getActivity(), "selectedIds");
        String recipesIds = StateHelper.getValue(getActivity(), "selectedRecipesIds");
        String titleTags = StateHelper.getValue(getActivity(), "selectedTags");

        tags = tags.replace("[","");
        tags = tags.replace("]","");

        titleTags = titleTags.replace("[","");
        titleTags = titleTags.replace("]","");

        recipesIds = recipesIds.replace("[","");
        recipesIds = recipesIds.replace("]","");

        String [] tag = tags.split(",");
        String [] titleTag = titleTags.split(",");
        String [] recipeIds = recipesIds.split(",");

        ArrayList<String> listTags = new ArrayList(Arrays.asList(tag));
        ArrayList<String> listTitleTags = new ArrayList(Arrays.asList(titleTag));
        ArrayList<String> listRecipesIds = new ArrayList(Arrays.asList(recipeIds));


        for(int i = 0; i < listTags.size(); i++){
            if(listTags.get(i) != "") {
                selectedItems.selectedIds.add(Integer.parseInt(listTags.get(i).trim()));
            }
            if(listTitleTags.size() > i && listTitleTags.get(i) != "") {
                selectedItems.selectedTags.add(listTitleTags.get(i).trim());
            }
        }

        for(int i = 0; i < listRecipesIds.size(); i++){
            if(listRecipesIds.get(i) != "")
                selectedItems.selectedRecipesIds.add(Integer.parseInt(listRecipesIds.get(i).trim()));
        }

        TextView resultTitle = (TextView) view.findViewById(R.id.result_title);
        TextView selectedTitle = (TextView) view.findViewById(R.id.selected_title);

        final ArrayAdapter<String> selectedAdapter = new TagsAdapter(getActivity(), resultTitle, selectedTitle);
        final ListView listSelectedTags = (ListView) view.findViewById(R.id.selected_tags);
        listSelectedTags.setAdapter(selectedAdapter);

        if(selectedItems.selectedIds.size() > 0){
            resultTitle.setVisibility(View.VISIBLE);
            selectedTitle.setVisibility(View.VISIBLE);

            Fragment fragmentListItem = ListItemFragment.getSearchInstance(selectedItems.selectedIds);
            FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
            frTransaction.replace(R.id.searched_items, fragmentListItem);
            frTransaction.commit();
        }else{
            resultTitle.setVisibility(View.GONE);
            selectedTitle.setVisibility(View.GONE);
        }

        ItemFragment.setListViewHeightBasedOnChildren(listSelectedTags);

    }

    @Override
    public void onPause(){
        super.onPause();

        StateHelper.setValue(getActivity(), "selectedIds", selectedItems.selectedIds.toString());
        StateHelper.setValue(getActivity(), "selectedTags", selectedItems.selectedTags.toString());
        StateHelper.setValue(getActivity(), "selectedRecipesIds", selectedItems.selectedRecipesIds.toString());
    }

    public interface SearchCallbacks {
        // TODO: Update argument type and name
        public void onSearchSelected(Uri uri);
    }
}
