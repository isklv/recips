package ru.garyk.recept;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import ru.garyk.recept.util.IabHelper;
import ru.garyk.recept.util.IabResult;
import ru.garyk.recept.util.Inventory;
import ru.garyk.recept.util.Purchase;


public class Main extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, CategoryFragment.CategoryCallbacks, ListItemFragment.ListItemCallbacks, SearchFragment.SearchCallbacks, AboutFragment.buyClick {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private Boolean premium = false;
    private Boolean blockClick = false;
    private Button buyButton;

    static final String ITEM_SKU = "recipes.purchased";
    private String TAG = "ru.garyk.recept";
    private IabHelper mHelper;

    private AlertDialog.Builder ad;
    private DatabaseHelper db;
    private FragmentTransaction frTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_head);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        ad = new AlertDialog.Builder(Main.this);
        ad.setTitle(R.string.app_name);
        ad.setMessage(R.string.close_app);
        ad.setCancelable(false);
        ad.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        ad.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });


        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmY/dhRMMeLJVXswZu8VIA2BxQJkXwFAxB3CiNa12r5kRNhsaHUwYXAOd/WuZok5jVYXULxnv9GAg4WE8JukIvDVf9LCTwYUlSXb3oN9Q3E4fd3GNlk2zJvMUIxORSc/9Bw/tTfWcti7lq72J01KfAvnA1l5vfTKIkNMCb8VH9EPfhxxh8TuR5V86WHiMp9Jk5WbCJAaDmvofz+c2QddfKFIxENjbTI3b+TOjxszEJUETuLC0wfMdlK//cv947yODfQvz8JeqO1M2C/QZbCm5TguWeGr8oP/vi6Qb61pVsBBEtZ0OjLbA9punqLJs/9NKwUN8enXCGIahS7fILmaiFwIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });

        getUpdate();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                // Handle error
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                buyButton.setEnabled(false);
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
    new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase,
                                      IabResult result) {

            if (result.isSuccess()) {
                premium = true;
                getUpdate();
                Toast.makeText(Main.this, R.string.premium_string, Toast.LENGTH_LONG).show();
                buyButton.setEnabled(true);
            } else {
                // handle error
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    private void getUpdate() {

        db = new DatabaseHelper(Main.this);
        SQLiteDatabase sdb = db.getReadableDatabase();
        Cursor cursor = sdb.query("recipes", new String[]{"id"}, null, null, null, null, null);

        if ((premium == true && cursor.getCount() == 30000) || cursor.getCount() >= 3000) {
            db.close();
            return;
        }


        final ProgressDialog pd = UIutil.progressDialog(this);
        pd.setCancelable(false);
        pd.setMessage(getString(R.string.upload_string));
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setProgress(0);
        pd.setMax(3000);
        pd.setIndeterminate(false);
        pd.show();

        new Thread(new Runnable() {
            public void run() {

            db = new DatabaseHelper(Main.this);

            Integer allRecipes = 0;
            Integer endRecipes = 1;

            while (endRecipes != 0) {

                SQLiteDatabase sdb = db.getReadableDatabase();

                int recipes = 0;
                int categories = 0;
                int tags = 0;

                Cursor cursor = sdb.query("recipes", new String[]{"id"}, null, null, null, null, "id DESC", "1");
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    recipes = cursor.getInt(0);
                }

                Cursor cursorCat = sdb.query("categories", new String[]{"id"}, null, null, null, null, "id DESC", "1");
                if (cursorCat.getCount() > 0) {
                    cursorCat.moveToFirst();
                    categories = cursorCat.getInt(0);
                }

                Cursor cursorTag = sdb.query("tags", new String[]{"id"}, null, null, null, null, "id DESC", "1");
                if (cursorTag.getCount() > 0) {
                    cursorTag.moveToFirst();
                    tags = cursorTag.getInt(0);
                }

                StringBuilder stringBuilder = new StringBuilder();
                HttpClient httpClient = new DefaultHttpClient();

                String queryPremium = (premium == true) ? "&premium=1" : "";

                HttpGet httpGet = new HttpGet("http://recipes.garyk.ru/?action=checkupdate&token=CD8JQB0v8KWrUr7Mqucs" + queryPremium + "&recipe=" + Integer.toString(recipes) + "&tag=" + Integer.toString(tags) + "&category=" + Integer.toString(categories));
                Log.i("query", "http://recipes.garyk.ru/?action=checkupdate&token=CD8JQB0v8KWrUr7Mqucs" + queryPremium + "&recipe=" + Integer.toString(recipes) + "&tag=" + Integer.toString(tags) + "&category=" + Integer.toString(categories));
                try {
                    HttpResponse response = httpClient.execute(httpGet);
                    StatusLine statusLine = response.getStatusLine();
                    int statusCode = statusLine.getStatusCode();

                    if (statusCode == 200) {
                        HttpEntity entity = response.getEntity();
                        InputStream inputStream = entity.getContent();
                        BufferedReader reader = new BufferedReader(
                                new InputStreamReader(inputStream));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                        inputStream.close();
                    } else {
                        Log.d("JSON", "Failed to download file");
                    }
                } catch (Exception e) {
                    //Toast.makeText(Main.this, R.string.not_connection, Toast.LENGTH_LONG).show();
                    Log.d("readJSONFeed", e.getLocalizedMessage());
                }
                String json = stringBuilder.toString();

                try {

                    JSONObject jArr = new JSONObject(json);

                    allRecipes = jArr.has("all") ? jArr.getInt("all") : 0;
                    endRecipes = jArr.has("end") ? jArr.getInt("end") : 0;

                    SQLiteDatabase wdb = db.getWritableDatabase();

                    JSONArray cArr = (jArr.has("categories")) ? jArr.getJSONArray("categories") : new JSONArray();
                    JSONArray rArr = (jArr.has("recipes")) ? jArr.getJSONArray("recipes") : new JSONArray();
                    JSONArray tArr = (jArr.has("tags")) ? jArr.getJSONArray("tags") : new JSONArray();
                    JSONArray rcArr = (jArr.has("recipes_categories")) ? jArr.getJSONArray("recipes_categories") : new JSONArray();
                    JSONArray rtArr = (jArr.has("recipes_tags")) ? jArr.getJSONArray("recipes_tags") : new JSONArray();

                    for (int i = 0; i < cArr.length(); i++) {
                        ContentValues values = new ContentValues();
                        values.put("id", cArr.getJSONObject(i).getString("id"));
                        values.put("name", cArr.getJSONObject(i).getString("name"));
                        wdb.insert("categories", null, values);
                    }

                    for (int i = 0; i < rArr.length(); i++) {
                        ContentValues values = new ContentValues();
                        values.put("id", rArr.getJSONObject(i).getString("id"));
                        values.put("name", rArr.getJSONObject(i).getString("name"));
                        values.put("descr", rArr.getJSONObject(i).getString("descr"));
                        values.put("time", rArr.getJSONObject(i).getString("time"));
                        //values.put("img", rArr.getJSONObject(i).getString("img"));
                        wdb.insert("recipes", null, values);
                    }

                    for (int i = 0; i < tArr.length(); i++) {
                        ContentValues values = new ContentValues();
                        values.put("id", tArr.getJSONObject(i).getString("id"));
                        values.put("value", tArr.getJSONObject(i).getString("value"));
                        wdb.insert("tags", null, values);
                    }

                    for (int i = 0; i < rcArr.length(); i++) {
                        ContentValues values = new ContentValues();
                        values.put("recipe_id", rcArr.getJSONObject(i).getString("recipe_id"));
                        values.put("category_id", rcArr.getJSONObject(i).getString("category_id"));
                        wdb.insert("recipes_categories", null, values);
                    }

                    for (int i = 0; i < rtArr.length(); i++) {
                        ContentValues values = new ContentValues();
                        values.put("recipe_id", rtArr.getJSONObject(i).getString("recipe_id"));
                        values.put("tag_id", rtArr.getJSONObject(i).getString("tag_id"));
                        values.put("count", rtArr.getJSONObject(i).getString("count"));
                        wdb.insert("recipes_tags", null, values);
                    }

                    wdb.close();

                    pd.setMax(allRecipes);
                    pd.setProgress(allRecipes - endRecipes);

                    Log.i("JSON", "update:process "+ allRecipes + ":" + endRecipes + ":" + (allRecipes-endRecipes));

                    if(endRecipes == 0)
                        pd.dismiss();

                } catch (JSONException e) {
                    Log.d("JSON", "Json string: " + json);
                    pd.dismiss();
                } finally {
                    db.close();
                }

            }

            }
        }).start();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        if(blockClick == true)
            return;

        frTransaction = getSupportFragmentManager().beginTransaction();
        frTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        onSectionAttached(position);

        String lastBackStackName = null;

        if(getSupportFragmentManager().getBackStackEntryCount() > 0)
            lastBackStackName = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount()-1).getName();

        switch(position){
            case 0:
                Fragment fragmentCat = getSupportFragmentManager().findFragmentByTag(CategoryFragment.F_TAG);
                if(fragmentCat == null){
                    fragmentCat = CategoryFragment.getInstance();
                }

                frTransaction.replace(R.id.container, fragmentCat, CategoryFragment.F_TAG);
                if(lastBackStackName != getString(R.string.title_recept))
                    frTransaction.addToBackStack(getString(R.string.title_recept));
            break;
            case 1:
                Fragment  fragmentSearch = getSupportFragmentManager().findFragmentByTag(SearchFragment.F_TAG);
                if(fragmentSearch == null){
                    fragmentSearch = SearchFragment.getInstance();
                }

                frTransaction.replace(R.id.container, fragmentSearch, SearchFragment.F_TAG);
                if(lastBackStackName != getString(R.string.title_search_recept))
                    frTransaction.addToBackStack(getString(R.string.title_search_recept));
            break;
            case 2:
                Fragment  fragmentAbout = getSupportFragmentManager().findFragmentByTag(AboutFragment.F_TAG);
                if(fragmentAbout == null){
                    fragmentAbout = AboutFragment.getInstance();
                }

                frTransaction.replace(R.id.container, fragmentAbout, AboutFragment.F_TAG);
                if(lastBackStackName != getString(R.string.title_about))
                    frTransaction.addToBackStack(getString(R.string.title_about));
            break;
        }

        frTransaction.commit();
    }

    @Override
    public void onCategoryItemSelected(Integer id, String title) {
        mTitle = title;
        restoreActionBar();

        ListItemFragment.F_TAG = title;

        Fragment  fragmentListItem = getSupportFragmentManager().findFragmentByTag(ListItemFragment.F_TAG);
        if(fragmentListItem == null){
            fragmentListItem = ListItemFragment.getInstance(id);
        }

        frTransaction = getSupportFragmentManager().beginTransaction();
        frTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        frTransaction.replace(R.id.container, fragmentListItem, ListItemFragment.F_TAG);
        frTransaction.addToBackStack(title);
        frTransaction.commit();
    }

    @Override
    public void onListItemSelected(Integer id, String title) {
        mTitle = title;
        restoreActionBar();

        ItemFragment.F_TAG = title;

        Fragment  fragmentItem = getSupportFragmentManager().findFragmentByTag(ItemFragment.F_TAG);
        if(fragmentItem == null){
            fragmentItem = ItemFragment.getInstance(id);
        }

        frTransaction = getSupportFragmentManager().beginTransaction();
        frTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        frTransaction.replace(R.id.container, fragmentItem, ItemFragment.F_TAG);
        frTransaction.addToBackStack(title);
        frTransaction.commit();
    }

    @Override
    public void onBuyClick(Button btnBuy) {
        buyButton = btnBuy;
        mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001, mPurchaseFinishedListener, "mypurchasetoken");
    }

    @Override
    public void onSearchSelected(Uri uri) {

    }

    public void onSectionAttached(int number) {

        switch (number) {
            case 0:
                mTitle = getString(R.string.title_recept);
                break;
            case 1:
                mTitle = getString(R.string.title_search_recept);
                break;
            case 2:
                mTitle = getString(R.string.title_about);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        LayoutInflater inflator = LayoutInflater.from(this);
        View v = inflator.inflate(R.layout.action_bar, null);

        ((TextView)v.findViewById(R.id.action_bar_title)).setText(mTitle);
        actionBar.setCustomView(v);
        //hide keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.share, menu);
            restoreActionBar();

            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1 ){
            mTitle = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount()-2).getName();
            restoreActionBar();
            getSupportFragmentManager().popBackStack();
        } else {
            AlertDialog alertDialog = ad.create();
            alertDialog.show();
        }
    }

}
