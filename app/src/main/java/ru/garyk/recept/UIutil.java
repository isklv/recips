package ru.garyk.recept;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Sokolov on 12.05.2015.
 */
public class UIutil {

    public static ProgressDialog progressDialog(Context context){
        ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle(R.string.app_name);
        pd.setIndeterminate(true);
        return pd;
    }

    public static String getImg(Integer id){
        StringBuilder stringBuilder = new StringBuilder();
        HttpClient httpClient = new DefaultHttpClient();


        HttpGet httpGet = new HttpGet("http://recipes.garyk.ru/?action=img&token=CD8JQB0v8KWrUr7Mqucs&recipe=" + Integer.toString(id));
        try {
            HttpResponse response = httpClient.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();
            }else{
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            //Toast.makeText(Main.this, R.string.not_connection, Toast.LENGTH_LONG).show();
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return stringBuilder.toString();
    }

    static class LoadImg extends AsyncTask<Integer, Void, String> {

        ImageView img;

        LoadImg(ImageView img){
            this.img = img;
        }

        @Override
        protected String doInBackground(Integer... params) {

            return UIutil.getImg(params[0]);
        }

        @Override
        protected void onPostExecute(String string) {

            try {
                JSONObject obj = new JSONObject(string);
                String dataImg = obj.getString("img");

                if(dataImg.length() > 0) {
                    byte[] decodedString = Base64.decode(dataImg, Base64.DEFAULT);
                    Bitmap bitmapImg = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    img.setImageBitmap(bitmapImg);
                }else{
                    img.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
