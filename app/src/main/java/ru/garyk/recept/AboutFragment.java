package ru.garyk.recept;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AboutFragment#getInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutFragment extends Fragment implements Button.OnClickListener{

    public buyClick mListener;
    public Button btnBuy;
    public static final String F_TAG = "about";

    public static AboutFragment getInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        btnBuy = (Button) view.findViewById(R.id.buyButton);

        btnBuy.setOnClickListener(this);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (buyClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CategoryCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view){
        if (null != mListener) {
            mListener.onBuyClick(btnBuy);
        }
    }

    public interface buyClick{
        public void onBuyClick(Button btnBuy);
    }
}
