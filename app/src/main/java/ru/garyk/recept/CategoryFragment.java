package ru.garyk.recept;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ru.garyk.recept.CategoryFragment.CategoryCallbacks}
 * interface.
 */
public class CategoryFragment extends Fragment implements AbsListView.OnItemClickListener {

    private SQLiteDatabase sdb;
    private ArrayList<Integer> ids = new ArrayList<Integer>();
    private ArrayList<String> listView = new ArrayList<String>();
    public static final String F_TAG = "category";

    public CategoryCallbacks mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;

    // TODO: Rename and change types of parameters
    public static CategoryFragment getInstance() {
        CategoryFragment fragment = new CategoryFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CategoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mAdapter = new ArrayAdapter<String>(getActivity(), R.layout.category_list, R.id.txt, listView);

        listView.clear();

        try{
            DatabaseHelper db = new DatabaseHelper(getActivity());
            sdb = db.getReadableDatabase();

            Cursor cursorCatElements = sdb.query("categories", new String[] {"id", "name"}, null, null, null, null, "id ASC", null);
            cursorCatElements.moveToFirst();
            while(cursorCatElements.isAfterLast() == false){
                ids.add(cursorCatElements.getInt(0));
                listView.add(Html.fromHtml(cursorCatElements.getString(1)).toString());
                cursorCatElements.moveToNext();
            }
        }catch (SQLiteException e){
            Log.e(getClass().getSimpleName(), "Could not create or Open the database");
        }finally {
            sdb.close();
        }

        View view = inflater.inflate(R.layout.fragment_category_grid, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CategoryCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CategoryCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onCategoryItemSelected(ids.get(position), listView.get(position));
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface CategoryCallbacks {
        // TODO: Update argument type and name
        public void onCategoryItemSelected(Integer id, String title);
    }

}
