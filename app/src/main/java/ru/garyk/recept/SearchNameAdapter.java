package ru.garyk.recept;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sokolov on 07.07.2015.
 */
public class SearchNameAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private SQLiteDatabase sdb;

    private final Context mContext;
    private List<Recipe> mResults;

    public SearchNameAdapter(Context context) {
        mContext = context;
        mResults = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public Recipe getItem(int index) {
        return mResults.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.simple_dropdown, parent, false);
        }
        Recipe recipe = getItem(position);
        ((TextView) convertView.findViewById(R.id.text)).setText(recipe.getTitle());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<Recipe> recipe = findRecipes(constraint.toString());
                    // Assign the data to the FilterResults
                    filterResults.values = recipe;
                    filterResults.count = recipe.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    mResults = (List<Recipe>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};

        return filter;
    }

    private List<Recipe> findRecipes(String findText) {

        List<Recipe> recipes = new ArrayList<>();

        try {
            DatabaseHelper db = new DatabaseHelper(mContext);
            sdb = db.getReadableDatabase();

            String query = "SELECT id, name FROM recipes WHERE name LIKE ? or name LIKE ?";
            Cursor cursor = sdb.rawQuery(query, new String[]{"%" + findText.toString() + "%", "%" + findText.toString().substring(0, 1).toUpperCase() + findText.toString().substring(1) + "%"});

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                Recipe rec = new Recipe(cursor.getInt(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("name")));
                recipes.add(rec);
                cursor.moveToNext();
            }

        } catch (SQLiteException e) {
            Log.e(getClass().getSimpleName(), "Could not create or Open the database");
        } finally {
            sdb.close();
        }

        return recipes;
    }

    class Recipe {

        private String name;
        private Integer id;

        Recipe(Integer id, String name){
            this.id = id;
            this.name = name;
        }

        public String getTitle(){
            return name;
        }

        public Integer getId(){
            return id;
        }
    }
}