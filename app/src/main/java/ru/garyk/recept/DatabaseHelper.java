package ru.garyk.recept;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Sokolov on 13.04.2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "recipes.db";
    private static final int DATABASE_VERSION = 4;
    private Context context;
    private static final String DATABASE_CREATE_CATEGORIES = "CREATE TABLE IF NOT EXISTS categories ("
            + " id integer primary key autoincrement,"
            + " name varchar(255));";
    private static final String DATABASE_CREATE_RECIPES = " CREATE TABLE IF NOT EXISTS recipes ("
            + " id integer primary key autoincrement,"
            + " name varchar(255),"
            + " time varchar(255),"
            + " descr text,"
            + " updated varchar(255));";
    private static final String DATABASE_CREATE_REC_CAT = " CREATE TABLE IF NOT EXISTS recipes_categories ("
            + " recipe_id integer,"
            + " category_id integer);";
    private static final String DATABASE_CREATE_REC_TAGS = " CREATE TABLE IF NOT EXISTS recipes_tags ("
            + " recipe_id integer,"
            + " tag_id integer,"
            + " count varchar(255));";
    private static final String DATABASE_CREATE_TAGS = " CREATE TABLE IF NOT EXISTS tags ("
            + " id integer primary key autoincrement,"
            + " value varchar(255));";
    private static final String DATABASE_CREATE_STATE = " CREATE TABLE IF NOT EXISTS state ("
            + " name varchar(255),"
            + " value varchar(255));";


    private static final String DATABASE_DROP_CATEGORIES = "drop table if exists categories;";
    private static final String DATABASE_DROP_RECIPES = "drop table if exists recipes;";
    private static final String DATABASE_DROP_REC_CAT = "drop table if exists recipes_categories;";
    private static final String DATABASE_DROP_REC_TAGS = "drop table if exists recipes_tags;";
    private static final String DATABASE_DROP_TAGS = "drop table if exists tags;";
    private static final String DATABASE_DROP_STATE = "drop table if exists state;";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_CATEGORIES);
        db.execSQL(DATABASE_CREATE_REC_CAT);
        db.execSQL(DATABASE_CREATE_REC_TAGS);
        db.execSQL(DATABASE_CREATE_RECIPES);
        db.execSQL(DATABASE_CREATE_TAGS);
        db.execSQL(DATABASE_CREATE_STATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Запишем в журнал
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);

        db.execSQL(DATABASE_DROP_CATEGORIES);
        db.execSQL(DATABASE_DROP_REC_CAT);
        db.execSQL(DATABASE_DROP_REC_TAGS);
        db.execSQL(DATABASE_DROP_RECIPES);
        db.execSQL(DATABASE_DROP_TAGS);
        db.execSQL(DATABASE_DROP_STATE);
        // Создаём новую таблицу
        onCreate(db);

    }


}
