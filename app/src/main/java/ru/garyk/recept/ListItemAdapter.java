package ru.garyk.recept;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sokolov on 28.04.2015.
 */
public class ListItemAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> title;
    private final ArrayList<Integer> ids;
    private int lastPosition = -1;

    public ListItemAdapter(Activity context, ArrayList<String> title, ArrayList<Integer> ids) {
        super(context, R.layout.list_single, title);
        this.context = context;
        this.title = title;
        this.ids = ids;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        MyImageView imageView = (MyImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(title.get(position));

        new UIutil.LoadImg(imageView).execute(ids.get(position));

        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        rowView.startAnimation(animation);
        lastPosition = position;

        return rowView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        lastPosition = -1;
    }
}
