package ru.garyk.recept;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ItemFragment#getInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String RECIPE_ID = "recipeId";
    private SQLiteDatabase sdb;
    private String text;
    private ShareActionProvider shareAction;
    Uri bmpUri;
    public static String F_TAG;

    // TODO: Rename and change types of parameters
    private Integer recipeId;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment ItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ItemFragment getInstance(Integer recipeId) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(RECIPE_ID, recipeId);
        fragment.setArguments(args);
        return fragment;
    }

    public ItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recipeId = getArguments().getInt(RECIPE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        TextView title = (TextView) view.findViewById(R.id.title);
        MyImageView img = (MyImageView) view.findViewById(R.id.img);
        TextView descr = (TextView) view.findViewById(R.id.descr);
        ListView tags = (ListView) view.findViewById(R.id.tags);

        try{
            DatabaseHelper db = new DatabaseHelper(getActivity());
            sdb = db.getReadableDatabase();

            String query = "SELECT * FROM recipes WHERE id = ?";
            Cursor cursorElements = sdb.rawQuery(query, new String[]{String.valueOf(recipeId)});

            cursorElements.moveToFirst();

            title.setText(Html.fromHtml(cursorElements.getString(cursorElements.getColumnIndex("name"))));
            descr.setText(Html.fromHtml(cursorElements.getString(cursorElements.getColumnIndex("descr"))));

            new UIutil.LoadImg(img).execute(recipeId);

            text = Html.fromHtml("<h1>"+cursorElements.getString(cursorElements.getColumnIndex("name")) + "</h1>" + cursorElements.getString(cursorElements.getColumnIndex("descr"))).toString();

        }catch (SQLiteException e){
            Log.e(getClass().getSimpleName(), "Could not create or Open the database");
        }finally {
            sdb.close();
        }


        ArrayList<String> listTags = new ArrayList<String>();

        ArrayAdapter<String> tagsAdapter = new ArrayAdapter(getActivity(), R.layout.category_list, R.id.txt, listTags);
        tags.setAdapter(tagsAdapter);

        try{
            DatabaseHelper db = new DatabaseHelper(getActivity());
            sdb = db.getReadableDatabase();

            String query = "SELECT t.id id, t.value value, r.count count FROM tags t INNER JOIN recipes_tags r ON r.tag_id = t.id WHERE r.recipe_id = ?";
            Cursor cursorTagElements = sdb.rawQuery(query, new String[]{String.valueOf(recipeId)});

            text += Html.fromHtml("<h2>"+getString(R.string.title_ingridients)+"</h2>").toString();
            cursorTagElements.moveToFirst();
            while(cursorTagElements.isAfterLast() == false){
                text += Html.fromHtml(cursorTagElements.getString(cursorTagElements.getColumnIndex("value")) + ": " + cursorTagElements.getString(cursorTagElements.getColumnIndex("count"))+"<br>").toString();

                listTags.add(Html.fromHtml(cursorTagElements.getString(cursorTagElements.getColumnIndex("value"))).toString() + ": " + cursorTagElements.getString(cursorTagElements.getColumnIndex("count")));

                cursorTagElements.moveToNext();
            }

            text += Html.fromHtml("<br>https://play.google.com/store/apps/details?id=ru.garyk.recept");

            this.setListViewHeightBasedOnChildren(tags);

        }catch (SQLiteException e){
            Log.e(getClass().getSimpleName(), "Could not create or Open the database");
        }finally {
            sdb.close();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){

        inflater.inflate(R.menu.share, menu);

        MenuItem shareItem = menu.findItem(R.id.share_button);
        shareAction = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/html");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        //shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);

        shareAction.setShareIntent(shareIntent);
        super.onCreateOptionsMenu(menu,inflater);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    /*
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + 100;
        listView.setLayoutParams(params);
    }*/

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if(listItem != null){
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += (listItem.getMeasuredHeight() < 300) ? listItem.getMeasuredHeight() : listItem.getMeasuredHeight()/10+10;
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

}
