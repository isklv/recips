package ru.garyk.recept;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProgressBarFragment extends Fragment {

    public static final String F_TAG = "progressBar";
    private Boolean text = false;

    public ProgressBarFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            text = getArguments().getBoolean(F_TAG);
        }
    }

    public static ProgressBarFragment getInstance(Boolean text){
        ProgressBarFragment fragment = new ProgressBarFragment();
        Bundle args = new Bundle();
        args.putBoolean(F_TAG, text);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_progress_bar, container, false);

        if(text == false){
            ((TextView) view.findViewById(R.id.progress_bar_text)).setVisibility(View.GONE);
        }
        return view;
    }


}
