package ru.garyk.recept;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ru.garyk.recept.ListItemFragment.ListItemCallbacks}
 * interface.
 */
public class ListItemFragment extends Fragment implements ListView.OnItemClickListener {

    private SQLiteDatabase sdb;
    private ArrayList<Integer> ids = new ArrayList<>();
    private ArrayList<Integer> tagsId = new ArrayList<>();
    private ArrayList<String> listTitle = new ArrayList<>();

    private final static String CATEGORY_ID = "CategoryId";
    private final static String TAGS_ID = "TagsId";
    private Integer categoryId = 0;

    public static String F_TAG;

    private ListItemCallbacks mListener;

    private ListView mListView;

    private ListItemAdapter mAdapter;

    public static ListItemFragment getInstance(Integer categoryId) {
        ListItemFragment fragment = new ListItemFragment();
        Bundle args = new Bundle();
        args.putInt(CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ListItemFragment getSearchInstance(ArrayList<Integer> tagsId) {
        ListItemFragment fragment = new ListItemFragment();
        Bundle args = new Bundle();
        args.putIntegerArrayList(TAGS_ID, tagsId);
        fragment.setArguments(args);
        return fragment;
    }

    public ListItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            categoryId = getArguments().getInt(CATEGORY_ID);
            tagsId = getArguments().getIntegerArrayList(TAGS_ID);

            if(tagsId == null)
                tagsId = new ArrayList<>();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listitem_grid, container, false);

        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        mAdapter = new ListItemAdapter(getActivity(), listTitle, ids);
        mListView.setAdapter(mAdapter);

        LoadResults loadResults = new LoadResults();
        loadResults.execute();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ListItemCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onListItemSelected(ids.get(position), listTitle.get(position));
        }
    }

    class LoadResults extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
            Fragment  fragmentPd = getFragmentManager().findFragmentByTag(ProgressBarFragment.F_TAG);
            if(fragmentPd == null){
                fragmentPd = ProgressBarFragment.getInstance(false);
            }

            frTransaction.replace(R.id.progress, fragmentPd, ProgressBarFragment.F_TAG).commit();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String query = "";

            try{

                DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());


                sdb = db.getReadableDatabase();
                Cursor cursorCatElements;

                if(categoryId != 0){
                    query = "SELECT r.id id, r.name name FROM recipes r INNER JOIN recipes_categories c ON r.id = c.recipe_id WHERE c.category_id = ?";
                    cursorCatElements = sdb.rawQuery(query, new String[]{String.valueOf(categoryId)});
                }else{
                    String whereIn = tagsId.toString();
                    whereIn = whereIn.replace("[","");
                    whereIn = whereIn.replace("]","");
                    whereIn = whereIn.replace(","," or t.tag_id =");
                    query = "SELECT r.id id, r.name name, count(DISTINCT t.recipe_id) FROM recipes_tags t INNER JOIN recipes r ON t.recipe_id=r.id WHERE t.tag_id = "+whereIn+" GROUP BY t.recipe_id HAVING COUNT(*) = "+tagsId.size();

                    cursorCatElements = sdb.rawQuery(query, new String[]{});
                }

                cursorCatElements.moveToFirst();

                while(cursorCatElements.isAfterLast() == false && cursorCatElements.getCount() > 0){

                    ids.add(cursorCatElements.getInt(cursorCatElements.getColumnIndex("id")));
                    listTitle.add(Html.fromHtml(cursorCatElements.getString(1)).toString());
                    cursorCatElements.moveToNext();
                }

            }catch (SQLiteException e){
                Log.e(getClass().getSimpleName(), query + "Could not create or Open the database");
            }finally {
                if(sdb != null)
                    sdb.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);

            mListView.setAdapter(mAdapter);

            if(categoryId == 0)
                ItemFragment.setListViewHeightBasedOnChildren(mListView);

            FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
            Fragment  fragmentPd = getFragmentManager().findFragmentByTag(ProgressBarFragment.F_TAG);
            if(fragmentPd == null){
                fragmentPd = ProgressBarFragment.getInstance(false);
            }

            frTransaction.remove(fragmentPd).commit();
        }
    }

    public interface ListItemCallbacks {
        void onListItemSelected(Integer id, String title);
    }

}
